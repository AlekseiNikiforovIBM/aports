# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=6.1.0
pkgrel=0
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	python3
	ansible-core
	py3-enrich
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-tenacity
	py3-tomli
	py3-typing-extensions
	py3-wcmatch
	py3-yaml
	py3-ansible-compat
	py3-resolvelib<0.6.0
	py3-resolvelib>=0.5.4
	"
makedepends="
	py3-build
	py3-installer
	py3-setuptools
	py3-wheel
	py3-setuptools_scm
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver.tar.gz"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	python3 -m build --skip-dependency-check --no-isolation --wheel
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
c1ca5abe8e5f441a86db6a4b4fa03c2d87ed2277151ada9af34c3d8af6934d2c1d4b6bffbc11d6cfb7cd7601c9e43a041fe8a2b940fa0630532df05004e11d15  ansible-lint-6.1.0.tar.gz
"
