# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer:
pkgname=ocaml-zed
_pkgname=zed
pkgver=3.1.0
pkgrel=0
pkgdesc="Abstract engine for text edition in OCaml"
url="https://github.com/ocaml-community/zed"
arch="all !riscv64" # restricted by ocaml aport
license="BSD-3-Clause"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	dune
	ocaml
	ocaml-camomile-dev
	ocaml-charinfo_width-dev
	ocaml-compiler-libs
	ocaml-findlib
	ocaml-react-dev
	ocaml-result-dev
	"
options="!check"  # no tests provided
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/ocaml-community/zed/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build --root . @install --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	# There's just a readme and changelog.
	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
d4457af15f970adb7b1ed543017e590fa0ecc47171d6dd520a1397f21ce0219caa30a9db7bedb76c0602d4f6d0f0684012cc33feafe18b848d3261a460dea10a  ocaml-zed-3.1.0.tar.gz
"
