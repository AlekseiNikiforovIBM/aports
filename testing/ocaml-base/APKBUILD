# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-base
_pkgname=base
pkgver=0.15.0
pkgrel=0
pkgdesc="Full standard library replacement for OCaml"
url="https://github.com/janestreet/base"
arch="all !riscv64" # restricted by ocaml
license="MIT"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	dune
	dune-configurator-dev
	ocaml
	ocaml-compiler-libs
	ocaml-csexp-dev
	ocaml-sexplib0-dev
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://ocaml.janestreet.com/ocaml-core/v${pkgver%.*}/files/base-v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-v$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build -p base @install --no-buffer --verbose
}

check() {
	dune runtest --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
838652994052b445a5090a8ad2b3bfb4130df42fc332ab295865000818de775cc78fb5b8960e29c68ab7cdff72186aad3b4d953088b956d9817213b0d24f4cff  ocaml-base-0.15.0.tar.gz
"
