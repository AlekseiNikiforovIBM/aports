# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-omod
_pkgname=omod
pkgver=0.0.3
pkgrel=0
pkgdesc="Lookup and load installed OCaml modules"
url="https://erratique.ch/software/omod"
arch="all !riscv64"  # limited by ocaml aport
license="ISC"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	ocaml
	ocaml-compiler-libs
	ocaml-findlib
	ocamlbuild
	ocaml-topkg
	ocaml-cmdliner-dev
	cmd:opam-installer
	"
options="!check"  # no tests provided
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.bz2::https://erratique.ch/software/omod/releases/omod-$pkgver.tbz"
builddir="$srcdir/$_pkgname-$pkgver"
_ocamldir=usr/lib/ocaml

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	ocaml pkg/pkg.ml build \
		--lib-dir "$(ocamlc -where)"
}

package() {
	opam-installer -i \
		--prefix="$pkgdir/usr" \
		--libdir="$pkgdir/$_ocamldir" \
		--docdir="$builddir/.omit" \
		$_pkgname.install
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find $_ocamldir \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
4f53b8cdd054dc1a6813427452a91294e0bbcfefe948fc1caec47be136dbcecf13112bf2b620fa2f667592b04b28df74e3bf012ea0fb0038c1da4217155ca626  ocaml-omod-0.0.3.tar.bz2
"
