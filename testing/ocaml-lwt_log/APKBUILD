# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer:
pkgname=ocaml-lwt_log
_pkgname=lwt_log
pkgver=1.1.1
pkgrel=0
pkgdesc="Lwt-friendly logger"
url="https://github.com/aantron/lwt_log"
arch="all !riscv64"  # limited by ocaml aport
license="LGPL-2.0-or-later"
depends="ocaml-runtime ocaml-lwt"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	dune
	dune-configurator-dev
	ocaml
	ocaml-bigarray-compat-dev
	ocaml-compiler-libs
	ocaml-cppo-dev
	ocaml-csexp-dev
	ocaml-findlib-dev
	ocaml-lwt-dev
	ocaml-mmap-dev
	ocaml-ocplib-endian-dev
	ocaml-react-dev
	ocaml-result-dev
	ocaml-seq-dev
	"
options="!check"  # no tests provided
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/aantron/lwt_log/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build --root . @install --no-buffer --verbose
}

check() {
	dune runtest --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	# There's just a readme and changelog.
	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
df3d171a7c72f37e96b756d252ab586767df9c13e01500faf13d4b2cee936b0602fd7c725c03db488d3737d8d92300af103d395a926dc654a2c44a5d6068f24a  ocaml-lwt_log-1.1.1.tar.gz
"
