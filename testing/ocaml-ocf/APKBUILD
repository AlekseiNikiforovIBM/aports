# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-ocf
_pkgname=ocf
pkgver=0.8.0
pkgrel=0
pkgdesc="OCaml library to read and write configuration files in JSON syntax"
url="https://zoggy.frama.io/ocf/"
arch="all !riscv64"  # restricted by ocaml
license="GPL-3.0-only"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	dune
	ocaml
	ocaml-compiler-libs
	ocaml-compiler-libs-repackaged-dev
	ocaml-ppxlib-dev
	ocaml-ppx_derivers-dev
	ocaml-sexplib0-dev
	ocaml-stdlib-shims
	ocaml-yojson-dev
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://framagit.org/zoggy/ocf/-/archive/$pkgver/ocf-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build --root . @install --no-buffer --verbose
}

check() {
	dune runtest --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
b9b1ce82ff370222b74429b28748c99b5b78c657e8c8b273d404ba9c17df2cd48274aad4d16b4748ecdbd1402cb8fad4136122cdf617b8ca1d6256a09efe2161  ocaml-ocf-0.8.0.tar.gz
"
