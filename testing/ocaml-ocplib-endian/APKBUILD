# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-ocplib-endian
_pkgname=ocplib-endian
pkgver=1.2
pkgrel=0
pkgdesc="Optimised functions to read and write int16/32/64 from strings and bigarrays"
url="https://github.com/OCamlPro/ocplib-endian"
arch="all !riscv64" # restricted by ocaml
license="LGPL-2.1-or-later WITH OCaml-LGPL-linking-exception"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="dune ocaml ocaml-cppo-dev"
options="!check"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/OCamlPro/ocplib-endian/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build --root . @install --no-buffer --verbose
}

check() {
	dune runtest --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
2e70be5f3d6e377485c60664a0e235c3b9b24a8d6b6a03895d092c6e40d53810bfe1f292ee69e5181ce6daa8a582bfe3d59f3af889f417134f658812be5b8b85  ocaml-ocplib-endian-1.2.tar.gz
"
